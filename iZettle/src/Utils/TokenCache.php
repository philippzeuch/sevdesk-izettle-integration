<?php

namespace Zeuch\iZettle\Utils;

use DateTime;

class TokenCache
{

    public static function storeTokens(string $endpoint, $access_token, $expires, $refresh_token = "")
    {
        $_SESSION[$endpoint]['access_token'] = $access_token;
        $_SESSION[$endpoint]['refresh_token'] = $refresh_token;
        $_SESSION[$endpoint]['token_expires'] = $expires;
        $_SESSION[$endpoint]['token_ausgestellt_am'] = new DateTime();
    }

    public static function tokenValid($endpoint): bool
    {
        /** @var DateTime $dateClone */
        $token_ausgestellt_am = $_SESSION[$endpoint]['token_ausgestellt_am'];
        if ($token_ausgestellt_am !== null) {
            $dateClone = clone $token_ausgestellt_am;
            return new DateTime() < $dateClone->modify("+" . $_SESSION[$endpoint]['token_expires'] . " second");
        }
        return false;
    }

    public function clearTokens($endpoint)
    {
        unset($_SESSION[$endpoint]['access_token']);
        unset($_SESSION[$endpoint]['refresh_token']);
        unset($_SESSION[$endpoint]['token_expires']);
    }

    public static function getAccessToken(string $endpoint)
    {
        return $_SESSION[$endpoint]['access_token'];
    }
}