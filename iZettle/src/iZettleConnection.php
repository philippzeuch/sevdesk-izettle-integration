<?php

namespace Zeuch\iZettle;

use Exception;
use Zeuch\iZettle\Utils\TokenCache;
use Composer\CaBundle\CaBundle;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class iZettleConnection
{
    private $client_id;

    private $client_secret;

    private $username;

    private $password;

    private $expires_in;

    public function __construct(string $client_id, string $client_secret, string $username, string $password)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->username = $username;
        $this->password = $password;

        if (empty($client_id) || empty($client_secret) || empty($username) || empty($password)) {
            throw new Exception("Es fehlen Zugangsdaten für die Verbindung zu Zettle. Bitte Überprüfe die Angaben.");
        }
    }

    public function connect() {
        $this->generateNewToken();
    }

    private function generateNewToken()
    {
        $client = new Client([
            'base_uri'             => "https://oauth.izettle.com",
            RequestOptions::VERIFY => CaBundle::getSystemCaRootBundlePath()
        ]);
        $response = null;
        try {
            $response = $client->request('POST', "/token", [
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => $this->client_id,
                    'client_secret' => $this->client_secret,
                    'username'      => $this->username,
                    'password'      => $this->password
                ],
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ]
            ]);
        } catch (GuzzleException $e) {

        }
        if ($response != null && $response->getStatusCode() == 200) {
            $json = $response->getBody()->getContents();
            $obj = json_decode($json);

            TokenCache::storeTokens("izettle", $obj->access_token, $obj->expires_in, $obj->refresh_token);
        }
    }

    public function getAccessToken()
    {
        $endpoint = "izettle";

        if (!TokenCache::tokenValid($endpoint)) {
            $this->generateNewToken();
        }
        return TokenCache::getAccessToken($endpoint);
    }
}