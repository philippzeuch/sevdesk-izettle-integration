<?php

namespace Zeuch\iZettle\Repo;

use Zeuch\iZettle\Model\iZettleAccountTransaction;
use DateTime;

class ZettleTransactionRepo extends iZettleRepo
{
    /**
     * @return iZettleAccountTransaction[]
     */
    public function getByZeitspanne(DateTime $beginn, DateTime $ende, array $types)
    {
        $client = $this->getRestClient();
        $queryParameters = [
            'start' => $beginn->format("Y-m-d\TH:m:s"),
            'end' => $ende->format("Y-m-d\TH:m:s")
        ];

        $response = $client->get("/organizations/self/accounts/LIQUID/transactions", [
            'query' => http_build_query($queryParameters) . self::transactionTypesAsUrlQueryParamsString($types)
        ]);
        return $this->getMultiple($response, iZettleAccountTransaction::class, "data");
    }

    private static function transactionTypesAsUrlQueryParamsString(array $types): string
    {
        $str = "";
        foreach ($types as $type) {
            $str .= "&includeTransactionType=" . $type;
        }
        return $str;
    }

    protected function getBaseUri(): string
    {
        return "https://finance.izettle.com";
    }
}