<?php

namespace Zeuch\iZettle\Repo;

use Zeuch\iZettle\Model\iZettlePurchase;
use DateTime;

class PurchaseRepo extends iZettleRepo
{
    public function getForYesterday()
    {
        return $this->getByZeitspanne(
            new DateTime('yesterday'),
            new DateTime('today')
        );
    }

    public function getForDay(int $year, int $month, int $day)
    {
        $beginn = new DateTime();
        $beginn->setDate($year, $month, $day);
        $beginn->setTime(0, 0);

        $ende = new DateTime();
        $ende->setDate($year, $month, $day + 1);
        $ende->setTime(0, 0);

        return $this->getByZeitspanne($beginn, $ende);
    }

    /**
     * @param DateTime $beginn
     * @param DateTime $ende
     * @return iZettlePurchase[]
     */
    public function getByZeitspanne(DateTime $beginn, DateTime $ende)
    {
        $client = $this->getRestClient();

        $response = $client->get("/purchases/v2", [
            'query' => [
                'startDate' => $beginn->format("Y-m-d\TH:m:s"),
                'endDate' => $ende->format("Y-m-d\TH:m:s")
            ]
        ]);
        return $this->getMultiple($response, iZettlePurchase::class, "purchases");
    }


    protected function getBaseUri(): string
    {
        return "https://purchase.izettle.com";
    }
}