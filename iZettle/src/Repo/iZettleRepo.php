<?php

namespace Zeuch\iZettle\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\iZettle\iZettleConnection;
use Composer\CaBundle\CaBundle;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use JsonMapper;
use Psr\Http\Message\ResponseInterface;

abstract class iZettleRepo
{
    /** @var iZettleConnection */
    protected $connection;

    public function __construct(ContainerInterface $c)
    {
        $this->connection = $c->get(iZettleConnection::class);
    }


    protected function getRestClient() {
        return new Client([
            'base_uri' => $this->getBaseUri(),
            RequestOptions::VERIFY => CaBundle::getSystemCaRootBundlePath(),
            'headers' => [
                'Authorization' => "Bearer {$this->connection->getAccessToken()}"
            ]
        ]);
    }

    /**
     * @param ResponseInterface $response
     * @param string $targetObjectClass
     * @param string $field
     * @return array|mixed
     */
    protected function getMultiple($response, string $targetObjectClass, $field = "") {
        $jsonDecoder = new JsonMapper();
        $objects = [];
        try {
            $json_decode = json_decode($response->getBody()->getContents());
            if (!empty($field)) {
                $objects = $jsonDecoder->mapArray($json_decode->{$field}, array(), $targetObjectClass);
            } else {
                $objects = $jsonDecoder->mapArray($json_decode, array(), $targetObjectClass);
            }
        } catch (\JsonMapper_Exception $e) {

        }

        return $objects;
    }

    protected abstract function getBaseUri() : string;
}