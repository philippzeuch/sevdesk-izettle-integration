<?php

namespace Zeuch\iZettle\Service;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\iZettle\Model\iZettlePurchase;

class ZettlePurchaseService
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(ContainerInterface $c)
    {
        $this->logger = $c->get(LoggerInterface::class);
    }

    public function purchaseContainsAnyDiscounts(iZettlePurchase $purchase): bool
    {
        if (!empty($purchase->getDiscounts())) {
            return true;
        }

        $positionenMitRabatten = array_filter($purchase->getProducts(), function ($product) {
            return $product->getDiscount() != null;
        });

        return !empty($positionenMitRabatten);
    }
}