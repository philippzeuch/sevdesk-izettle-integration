<?php

namespace Zeuch\iZettle\Service;

use Zeuch\iZettle\Model\iZettleProdukt;

class iZettleProduktService
{

    /**
     * Zahlen alle in integer Schreibweise a la iZettle
     * @param iZettleProdukt[] $produkte
     * @return int|null
     */
    public function getAmountOfProducts($produkte)
    {
        $amount = 0;
        foreach ($produkte as $produkt) {
            $amount += $this->getProduktAmount($produkt);
        }
        return $amount;
    }

    /**
     * Zahlen alle in integer Schreibweise a la iZettle
     * @param iZettleProdukt $produkt
     * @return float|int|null
     */
    public function getProduktAmount(iZettleProdukt $produkt)
    {
        $produktPreis = $produkt->getUnitPrice();
        $discount = $produkt->getDiscount();
        if ($discount !== null) {
            $produktPreis -= $discount->getPercentage() == null ? $discount->getAmount() : $produktPreis * ($discount->getPercentage() / 100);
        }

        return $produktPreis;
    }
}