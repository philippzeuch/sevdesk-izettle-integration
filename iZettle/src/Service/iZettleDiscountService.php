<?php

namespace Zeuch\iZettle\Service;

use Zeuch\iZettle\Model\iZettleDiscount;
use Zeuch\iZettle\Model\iZettleProdukt;

class iZettleDiscountService
{
    /**
     * Zahlen alle in integer Schreibweise a la iZettle
     * @param int $grossAmountTotal
     * @param iZettleDiscount[] $positionsDiscounts
     * @param iZettleProdukt[] $produktDiscounts
     * @return int|null
     */
    public function getSum($grossAmountTotal, $positionsDiscounts, $produktDiscounts)
    {
        $sumDiscounts = 0;
        foreach ($positionsDiscounts as $discount) {
            $sumDiscounts += $discount->getPercentage() == null ? $discount->getAmount() : $grossAmountTotal * ($discount->getPercentage() / 100);
        }
        foreach ($produktDiscounts as $produkt) {
            $discount = $produkt->getDiscount();
            if ($discount !== null) {
                $sumDiscounts += $discount->getPercentage() == null ? $discount->getAmount() : $produkt->getUnitPrice() * ($discount->getPercentage() / 100);
            }
        }
        return $sumDiscounts;
    }
}