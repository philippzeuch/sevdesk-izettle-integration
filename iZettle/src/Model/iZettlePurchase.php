<?php

namespace Zeuch\iZettle\Model;

use DateTime;

class iZettlePurchase
{
    /** @var string|null */
    private $source;

    /** @var string|null */
    private $purchaseUUID;

    /** @var int|null */
    private $amount;

    /** @var int|null */
    private $vatAmount;

    /** @var string|null */
    private $country;

    /** @var string|null */
    private $currency;

    /** @var DateTime|null */
    private $timestamp;

    /** @var iZettleGpsCoordinates|null */
    private $gpsCoordinates;

    /** @var int|null */
    private $purchaseNumber;

    /** @var int|null */
    private $globalPurchaseNumber;

    /** @var string|null */
    private $userDisplayName;

    /** @var int|null */
    private $userId;

    /** @var int|null */
    private $organizationId;

    /** @var iZettleProdukt[]|null */
    private $products;

    /** @var iZettleDiscount[]|null */
    private $discounts;

    /** @var iZettleServiceCharge|null */
    private $serviceCharge;

    /** @var iZettleCardPayment[]|null */
    private $cardPayments;

    /** @var iZettleCashPayment[]|null */
    private $cashPayments;

    /** @var iZettlePayment[]|null */
    private $payments;

    /** @var string[]|null */
    private $refundedByPurchaseUUIDs;

    /** @var string|null */
    private $refundsPurchaseUUID;

    /** @var iZettleCashRegisterDetails|null */
    private $cashRegister;

    /** @var bool|null */
    private $receiptCopyAllowed;

    /** @var int|null */
    private $gratuityAmount;

    /** @var object|null */
    private $references;

    /** @var iZettleRemoteSaleInformation|null */
    private $remoteSaleInformation;

    /** @var DateTime|null */
    private $created;

    /** @var array|null */
    private $groupedVatAmounts;

    /** @var string|null */
    private $refundsPurchaseUUID1;

    /** @var bool|null */
    private $refunded;

    /** @var string|null */
    private $purchaseUUID1;

    /** @var string[]|null */
    private $refundedByPurchaseUUIDs1;

    /** @var bool|null */
    private $refund;

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string|null
     */
    public function getPurchaseUUID(): ?string
    {
        return $this->purchaseUUID;
    }

    /**
     * @param string|null $purchaseUUID
     */
    public function setPurchaseUUID(?string $purchaseUUID): void
    {
        $this->purchaseUUID = $purchaseUUID;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return int|null
     */
    public function getVatAmount(): ?int
    {
        return $this->vatAmount;
    }

    /**
     * @param int|null $vatAmount
     */
    public function setVatAmount(?int $vatAmount): void
    {
        $this->vatAmount = $vatAmount;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return DateTime|null
     */
    public function getTimestamp(): ?DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime|null $timestamp
     */
    public function setTimestamp(?DateTime $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return iZettleGpsCoordinates|null
     */
    public function getGpsCoordinates(): ?iZettleGpsCoordinates
    {
        return $this->gpsCoordinates;
    }

    /**
     * @param iZettleGpsCoordinates|null $gpsCoordinates
     */
    public function setGpsCoordinates(?iZettleGpsCoordinates $gpsCoordinates): void
    {
        $this->gpsCoordinates = $gpsCoordinates;
    }

    /**
     * @return int|null
     */
    public function getPurchaseNumber(): ?int
    {
        return $this->purchaseNumber;
    }

    /**
     * @param int|null $purchaseNumber
     */
    public function setPurchaseNumber(?int $purchaseNumber): void
    {
        $this->purchaseNumber = $purchaseNumber;
    }

    /**
     * @return int|null
     */
    public function getGlobalPurchaseNumber(): ?int
    {
        return $this->globalPurchaseNumber;
    }

    /**
     * @param int|null $globalPurchaseNumber
     */
    public function setGlobalPurchaseNumber(?int $globalPurchaseNumber): void
    {
        $this->globalPurchaseNumber = $globalPurchaseNumber;
    }

    /**
     * @return string|null
     */
    public function getUserDisplayName(): ?string
    {
        return $this->userDisplayName;
    }

    /**
     * @param string|null $userDisplayName
     */
    public function setUserDisplayName(?string $userDisplayName): void
    {
        $this->userDisplayName = $userDisplayName;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    public function getOrganizationId(): ?int
    {
        return $this->organizationId;
    }

    /**
     * @param int|null $organizationId
     */
    public function setOrganizationId(?int $organizationId): void
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return iZettleProdukt[]|null
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param iZettleProdukt[]|null $products
     */
    public function setProducts(?array $products): void
    {
        $this->products = $products;
    }

    /**
     * @return iZettleDiscount[]|null
     */
    public function getDiscounts(): ?array
    {
        return $this->discounts;
    }

    /**
     * @param iZettleDiscount[]|null $discounts
     */
    public function setDiscounts(?array $discounts): void
    {
        $this->discounts = $discounts;
    }

    /**
     * @return iZettleServiceCharge|null
     */
    public function getServiceCharge(): ?iZettleServiceCharge
    {
        return $this->serviceCharge;
    }

    /**
     * @param iZettleServiceCharge|null $serviceCharge
     */
    public function setServiceCharge(?iZettleServiceCharge $serviceCharge): void
    {
        $this->serviceCharge = $serviceCharge;
    }

    /**
     * @return iZettleCardPayment[]|null
     */
    public function getCardPayments(): ?array
    {
        return $this->cardPayments;
    }

    /**
     * @param iZettleCardPayment[]|null $cardPayments
     */
    public function setCardPayments(?array $cardPayments): void
    {
        $this->cardPayments = $cardPayments;
    }

    /**
     * @return iZettleCashPayment[]|null
     */
    public function getCashPayments(): ?array
    {
        return $this->cashPayments;
    }

    /**
     * @param iZettleCashPayment[]|null $cashPayments
     */
    public function setCashPayments(?array $cashPayments): void
    {
        $this->cashPayments = $cashPayments;
    }

    /**
     * @return iZettlePayment[]|null
     */
    public function getPayments(): ?array
    {
        return $this->payments;
    }

    /**
     * @param iZettlePayment[]|null $payments
     */
    public function setPayments(?array $payments): void
    {
        $this->payments = $payments;
    }

    /**
     * @return string[]|null
     */
    public function getRefundedByPurchaseUUIDs(): ?array
    {
        return $this->refundedByPurchaseUUIDs;
    }

    /**
     * @param string[]|null $refundedByPurchaseUUIDs
     */
    public function setRefundedByPurchaseUUIDs(?array $refundedByPurchaseUUIDs): void
    {
        $this->refundedByPurchaseUUIDs = $refundedByPurchaseUUIDs;
    }

    /**
     * @return string|null
     */
    public function getRefundsPurchaseUUID(): ?string
    {
        return $this->refundsPurchaseUUID;
    }

    /**
     * @param string|null $refundsPurchaseUUID
     */
    public function setRefundsPurchaseUUID(?string $refundsPurchaseUUID): void
    {
        $this->refundsPurchaseUUID = $refundsPurchaseUUID;
    }

    /**
     * @return iZettleCashRegisterDetails|null
     */
    public function getCashRegister(): ?iZettleCashRegisterDetails
    {
        return $this->cashRegister;
    }

    /**
     * @param iZettleCashRegisterDetails|null $cashRegister
     */
    public function setCashRegister(?iZettleCashRegisterDetails $cashRegister): void
    {
        $this->cashRegister = $cashRegister;
    }

    /**
     * @return bool|null
     */
    public function getReceiptCopyAllowed(): ?bool
    {
        return $this->receiptCopyAllowed;
    }

    /**
     * @param bool|null $receiptCopyAllowed
     */
    public function setReceiptCopyAllowed(?bool $receiptCopyAllowed): void
    {
        $this->receiptCopyAllowed = $receiptCopyAllowed;
    }

    /**
     * @return int|null
     */
    public function getGratuityAmount(): ?int
    {
        return $this->gratuityAmount;
    }

    /**
     * @param int|null $gratuityAmount
     */
    public function setGratuityAmount(?int $gratuityAmount): void
    {
        $this->gratuityAmount = $gratuityAmount;
    }

    /**
     * @return object|null
     */
    public function getReferences(): ?object
    {
        return $this->references;
    }

    /**
     * @param object|null $references
     */
    public function setReferences(?object $references): void
    {
        $this->references = $references;
    }

    /**
     * @return iZettleRemoteSaleInformation|null
     */
    public function getRemoteSaleInformation(): ?iZettleRemoteSaleInformation
    {
        return $this->remoteSaleInformation;
    }

    /**
     * @param iZettleRemoteSaleInformation|null $remoteSaleInformation
     */
    public function setRemoteSaleInformation(?iZettleRemoteSaleInformation $remoteSaleInformation): void
    {
        $this->remoteSaleInformation = $remoteSaleInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime|null $created
     */
    public function setCreated(?DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return array|null
     */
    public function getGroupedVatAmounts(): ?array
    {
        return $this->groupedVatAmounts;
    }

    /**
     * @param array|null $groupedVatAmounts
     */
    public function setGroupedVatAmounts(?array $groupedVatAmounts): void
    {
        $this->groupedVatAmounts = $groupedVatAmounts;
    }

    /**
     * @return string|null
     */
    public function getRefundsPurchaseUUID1(): ?string
    {
        return $this->refundsPurchaseUUID1;
    }

    /**
     * @param string|null $refundsPurchaseUUID1
     */
    public function setRefundsPurchaseUUID1(?string $refundsPurchaseUUID1): void
    {
        $this->refundsPurchaseUUID1 = $refundsPurchaseUUID1;
    }

    /**
     * @return bool|null
     */
    public function getRefunded(): ?bool
    {
        return $this->refunded;
    }

    /**
     * @param bool|null $refunded
     */
    public function setRefunded(?bool $refunded): void
    {
        $this->refunded = $refunded;
    }

    /**
     * @return string|null
     */
    public function getPurchaseUUID1(): ?string
    {
        return $this->purchaseUUID1;
    }

    /**
     * @param string|null $purchaseUUID1
     */
    public function setPurchaseUUID1(?string $purchaseUUID1): void
    {
        $this->purchaseUUID1 = $purchaseUUID1;
    }

    /**
     * @return string[]|null
     */
    public function getRefundedByPurchaseUUIDs1(): ?array
    {
        return $this->refundedByPurchaseUUIDs1;
    }

    /**
     * @param string[]|null $refundedByPurchaseUUIDs1
     */
    public function setRefundedByPurchaseUUIDs1(?array $refundedByPurchaseUUIDs1): void
    {
        $this->refundedByPurchaseUUIDs1 = $refundedByPurchaseUUIDs1;
    }

    /**
     * @return bool|null
     */
    public function getRefund(): ?bool
    {
        return $this->refund;
    }

    /**
     * @param bool|null $refund
     */
    public function setRefund(?bool $refund): void
    {
        $this->refund = $refund;
    }
}