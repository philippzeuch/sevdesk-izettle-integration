<?php

namespace Zeuch\iZettle\Model;

class iZettlePayment
{
    /** @var string|null */
    private $uuid;

    /** @var string|null */
    private $receiverOrganization;

    /** @var int|null */
    private $amount;

    /** @var string|null */
    private $type;

    /** @var int|null */
    private $gratuityAmount;

    /** @var string|null */
    private $currency;

    /** @var string|null */
    private $country;

    /** @var string|null */
    private $referenceNumber;

    /** @var object|null */
    private $references;

    /** @var object|null */
    private $commission;

    /** @var int|null */
    private $createdAt;

    /** @var object|null */
    private $details;

    /** @var object|null */
    private $attributes;

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * @param string|null $uuid
     */
    public function setUuid(?string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string|null
     */
    public function getReceiverOrganization(): ?string
    {
        return $this->receiverOrganization;
    }

    /**
     * @param string|null $receiverOrganization
     */
    public function setReceiverOrganization(?string $receiverOrganization): void
    {
        $this->receiverOrganization = $receiverOrganization;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getGratuityAmount(): ?int
    {
        return $this->gratuityAmount;
    }

    /**
     * @param int|null $gratuityAmount
     */
    public function setGratuityAmount(?int $gratuityAmount): void
    {
        $this->gratuityAmount = $gratuityAmount;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }

    /**
     * @param string|null $referenceNumber
     */
    public function setReferenceNumber(?string $referenceNumber): void
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return object|null
     */
    public function getReferences(): ?object
    {
        return $this->references;
    }

    /**
     * @param object|null $references
     */
    public function setReferences(?object $references): void
    {
        $this->references = $references;
    }

    /**
     * @return object|null
     */
    public function getCommission(): ?object
    {
        return $this->commission;
    }

    /**
     * @param object|null $commission
     */
    public function setCommission(?object $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return int|null
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    /**
     * @param int|null $createdAt
     */
    public function setCreatedAt(?int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return object|null
     */
    public function getDetails(): ?object
    {
        return $this->details;
    }

    /**
     * @param object|null $details
     */
    public function setDetails(?object $details): void
    {
        $this->details = $details;
    }

    /**
     * @return object|null
     */
    public function getAttributes(): ?object
    {
        return $this->attributes;
    }

    /**
     * @param object|null $attributes
     */
    public function setAttributes(?object $attributes): void
    {
        $this->attributes = $attributes;
    }
}