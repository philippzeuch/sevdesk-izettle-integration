<?php

namespace Zeuch\iZettle\Model;

class iZettleServiceCharge
{
    /** @var string|null */
    private $title;

    /** @var int|null */
    private $amount;

    /** @var float|null */
    private $percentage;

    /** @var float|null */
    private $vatPercentage;

    /** @var int|null */
    private $quantity;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float|null
     */
    public function getPercentage(): ?float
    {
        return $this->percentage;
    }

    /**
     * @param float|null $percentage
     */
    public function setPercentage(?float $percentage): void
    {
        $this->percentage = $percentage;
    }

    /**
     * @return float|null
     */
    public function getVatPercentage(): ?float
    {
        return $this->vatPercentage;
    }

    /**
     * @param float|null $vatPercentage
     */
    public function setVatPercentage(?float $vatPercentage): void
    {
        $this->vatPercentage = $vatPercentage;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }
}