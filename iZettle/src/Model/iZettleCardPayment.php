<?php

namespace Zeuch\iZettle\Model;

class iZettleCardPayment
{
    /** @var string|null */
    private $cardPaymentUUID;

    /** @var int|null */
    private $amount;

    /** @var string|null */
    private $cardType;

    /** @var int|null */
    private $gratuityAmount;

    /** @var string|null */
    private $maskedPan;

    /** @var string|null */
    private $cardPaymentEntryMode;

    /** @var string|null */
    private $applicationIdentifier;

    /** @var string|null */
    private $applicationName;

    /** @var string|null */
    private $terminalVerificationResults;

    /** @var string|null */
    private $transactionStatusInformation;

    /** @var string|null */
    private $referenceNumber;

    /** @var int|null */
    private $nrOfInstallments;

    /** @var int|null */
    private $installmentAmount;

    /** @var string|null */
    private $cardPaymentUUID1;

    /**
     * @return string|null
     */
    public function getCardPaymentUUID(): ?string
    {
        return $this->cardPaymentUUID;
    }

    /**
     * @param string|null $cardPaymentUUID
     */
    public function setCardPaymentUUID(?string $cardPaymentUUID): void
    {
        $this->cardPaymentUUID = $cardPaymentUUID;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return $this->cardType;
    }

    /**
     * @param string|null $cardType
     */
    public function setCardType(?string $cardType): void
    {
        $this->cardType = $cardType;
    }

    /**
     * @return int|null
     */
    public function getGratuityAmount(): ?int
    {
        return $this->gratuityAmount;
    }

    /**
     * @param int|null $gratuityAmount
     */
    public function setGratuityAmount(?int $gratuityAmount): void
    {
        $this->gratuityAmount = $gratuityAmount;
    }

    /**
     * @return string|null
     */
    public function getMaskedPan(): ?string
    {
        return $this->maskedPan;
    }

    /**
     * @param string|null $maskedPan
     */
    public function setMaskedPan(?string $maskedPan): void
    {
        $this->maskedPan = $maskedPan;
    }

    /**
     * @return string|null
     */
    public function getCardPaymentEntryMode(): ?string
    {
        return $this->cardPaymentEntryMode;
    }

    /**
     * @param string|null $cardPaymentEntryMode
     */
    public function setCardPaymentEntryMode(?string $cardPaymentEntryMode): void
    {
        $this->cardPaymentEntryMode = $cardPaymentEntryMode;
    }

    /**
     * @return string|null
     */
    public function getApplicationIdentifier(): ?string
    {
        return $this->applicationIdentifier;
    }

    /**
     * @param string|null $applicationIdentifier
     */
    public function setApplicationIdentifier(?string $applicationIdentifier): void
    {
        $this->applicationIdentifier = $applicationIdentifier;
    }

    /**
     * @return string|null
     */
    public function getApplicationName(): ?string
    {
        return $this->applicationName;
    }

    /**
     * @param string|null $applicationName
     */
    public function setApplicationName(?string $applicationName): void
    {
        $this->applicationName = $applicationName;
    }

    /**
     * @return string|null
     */
    public function getTerminalVerificationResults(): ?string
    {
        return $this->terminalVerificationResults;
    }

    /**
     * @param string|null $terminalVerificationResults
     */
    public function setTerminalVerificationResults(?string $terminalVerificationResults): void
    {
        $this->terminalVerificationResults = $terminalVerificationResults;
    }

    /**
     * @return string|null
     */
    public function getTransactionStatusInformation(): ?string
    {
        return $this->transactionStatusInformation;
    }

    /**
     * @param string|null $transactionStatusInformation
     */
    public function setTransactionStatusInformation(?string $transactionStatusInformation): void
    {
        $this->transactionStatusInformation = $transactionStatusInformation;
    }

    /**
     * @return string|null
     */
    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }

    /**
     * @param string|null $referenceNumber
     */
    public function setReferenceNumber(?string $referenceNumber): void
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return int|null
     */
    public function getNrOfInstallments(): ?int
    {
        return $this->nrOfInstallments;
    }

    /**
     * @param int|null $nrOfInstallments
     */
    public function setNrOfInstallments(?int $nrOfInstallments): void
    {
        $this->nrOfInstallments = $nrOfInstallments;
    }

    /**
     * @return int|null
     */
    public function getInstallmentAmount(): ?int
    {
        return $this->installmentAmount;
    }

    /**
     * @param int|null $installmentAmount
     */
    public function setInstallmentAmount(?int $installmentAmount): void
    {
        $this->installmentAmount = $installmentAmount;
    }

    /**
     * @return string|null
     */
    public function getCardPaymentUUID1(): ?string
    {
        return $this->cardPaymentUUID1;
    }

    /**
     * @param string|null $cardPaymentUUID1
     */
    public function setCardPaymentUUID1(?string $cardPaymentUUID1): void
    {
        $this->cardPaymentUUID1 = $cardPaymentUUID1;
    }
}