<?php

namespace Zeuch\iZettle\Model;

use DateTime;

class iZettleAccountTransaction
{
    /** @var DateTime|null */
    private $timestamp;

    /** @var int|null */
    private $amount;

    /** @var string|null */
    private $originatorTransactionType;

    /** @var string|null */
    private $originatingTransactionUuid;

    /**
     * @return DateTime|null
     */
    public function getTimestamp(): ?DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime|null $timestamp
     */
    public function setTimestamp(?DateTime $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getOriginatorTransactionType(): ?string
    {
        return $this->originatorTransactionType;
    }

    /**
     * @param string|null $originatorTransactionType
     */
    public function setOriginatorTransactionType(?string $originatorTransactionType): void
    {
        $this->originatorTransactionType = $originatorTransactionType;
    }

    /**
     * @return string|null
     */
    public function getOriginatingTransactionUuid(): ?string
    {
        return $this->originatingTransactionUuid;
    }

    /**
     * @param string|null $originatingTransactionUuid
     */
    public function setOriginatingTransactionUuid(?string $originatingTransactionUuid): void
    {
        $this->originatingTransactionUuid = $originatingTransactionUuid;
    }
}