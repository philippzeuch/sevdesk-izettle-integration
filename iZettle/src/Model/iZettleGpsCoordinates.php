<?php

namespace Zeuch\iZettle\Model;

class iZettleGpsCoordinates
{
    /** @var float|null */
    private $longitude;

    /** @var float|null */
    private $latitude;

    /** @var float|null */
    private $accuracyMeters;

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     */
    public function setLongitude(?float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     */
    public function setLatitude(?float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float|null
     */
    public function getAccuracyMeters(): ?float
    {
        return $this->accuracyMeters;
    }

    /**
     * @param float|null $accuracyMeters
     */
    public function setAccuracyMeters(?float $accuracyMeters): void
    {
        $this->accuracyMeters = $accuracyMeters;
    }
}