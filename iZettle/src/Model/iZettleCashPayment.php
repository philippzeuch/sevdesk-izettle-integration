<?php

namespace Zeuch\iZettle\Model;

class iZettleCashPayment
{
    /** @var string|null */
    private $cashPaymentUUID;

    /** @var int|null */
    private $amount;

    /** @var int|null */
    private $handedAmount;

    /** @var int|null */
    private $gratuityAmount;

    /** @var string|null */
    private $cashPaymentUUID1;

    /**
     * @return string|null
     */
    public function getCashPaymentUUID(): ?string
    {
        return $this->cashPaymentUUID;
    }

    /**
     * @param string|null $cashPaymentUUID
     */
    public function setCashPaymentUUID(?string $cashPaymentUUID): void
    {
        $this->cashPaymentUUID = $cashPaymentUUID;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return int|null
     */
    public function getHandedAmount(): ?int
    {
        return $this->handedAmount;
    }

    /**
     * @param int|null $handedAmount
     */
    public function setHandedAmount(?int $handedAmount): void
    {
        $this->handedAmount = $handedAmount;
    }

    /**
     * @return int|null
     */
    public function getGratuityAmount(): ?int
    {
        return $this->gratuityAmount;
    }

    /**
     * @param int|null $gratuityAmount
     */
    public function setGratuityAmount(?int $gratuityAmount): void
    {
        $this->gratuityAmount = $gratuityAmount;
    }

    /**
     * @return string|null
     */
    public function getCashPaymentUUID1(): ?string
    {
        return $this->cashPaymentUUID1;
    }

    /**
     * @param string|null $cashPaymentUUID1
     */
    public function setCashPaymentUUID1(?string $cashPaymentUUID1): void
    {
        $this->cashPaymentUUID1 = $cashPaymentUUID1;
    }
}