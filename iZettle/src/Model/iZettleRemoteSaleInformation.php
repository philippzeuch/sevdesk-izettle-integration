<?php

namespace Zeuch\iZettle\Model;

class iZettleRemoteSaleInformation
{
    /** @var string|null */
    private $firstName;

    /** @var string|null */
    private $lastName;

    /** @var string|null */
    private $phone;

    /** @var string|null */
    private $email;

    /** @var iZettleAddress|null */
    private $deliveryAddress;

    /** @var iZettleAddress|null */
    private $billingAddress;

    /** @var iZettleCustomField[]|null */
    private $customFields;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return iZettleAddress|null
     */
    public function getDeliveryAddress(): ?iZettleAddress
    {
        return $this->deliveryAddress;
    }

    /**
     * @param iZettleAddress|null $deliveryAddress
     */
    public function setDeliveryAddress(?iZettleAddress $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return iZettleAddress|null
     */
    public function getBillingAddress(): ?iZettleAddress
    {
        return $this->billingAddress;
    }

    /**
     * @param iZettleAddress|null $billingAddress
     */
    public function setBillingAddress(?iZettleAddress $billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return iZettleCustomField[]|null
     */
    public function getCustomFields(): ?array
    {
        return $this->customFields;
    }

    /**
     * @param iZettleCustomField[]|null $customFields
     */
    public function setCustomFields(?array $customFields): void
    {
        $this->customFields = $customFields;
    }
}