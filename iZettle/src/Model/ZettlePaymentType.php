<?php

namespace Zeuch\iZettle\Model;

class ZettlePaymentType
{
    public static $CARD = "IZETTLE_CARD";

    public static $CASH = "IZETTLE_CASH";

    public static $INVOICE = "IZETTLE_INVOICE";

    public static $CARD_ONLINE = "IZETTLE_CARD_ONLINE";

    public static $KLARNA = "KLARNA";

    public static $SWISH = "SWISH";

    public static $VIPPS = "VIPPS";

    public static $MOBILE = "MOBILE_PAY";

    public static $PAYPAL = "PAYPAL";

    public static $STORE_CREDIT = "STORE_CREDIT";

    public static $GIFTCARD = "GIFTCARD";
}