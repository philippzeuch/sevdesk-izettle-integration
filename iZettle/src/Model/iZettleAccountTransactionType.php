<?php

namespace Zeuch\iZettle\Model;

class iZettleAccountTransactionType
{
    static $CARD_PAYMENT = "CARD_PAYMENT";

    static $CARD_PAYMENT_FEE = "CARD_PAYMENT_FEE";

    static $PAYOUT = "PAYOUT";
}