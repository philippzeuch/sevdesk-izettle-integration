# sevDesk Zettle Integration

Da es für beide Systeme noch keine Plugins zur Verknüpfung etc. gibt 
und es auf beiden Seiten eine recht einfache API gibt habe ich begonnen 
in PHP eine Schnittstelle zwischen beiden Systemen zusammenzubasteln. 
Ich bin in PHP recht unerfahren und freue mich über jede Unterstützung per Pull-Requests.

#### Was ist sevDesk?
sevDesk ist eine mandantenfähige WebApp zur Finanzbuchhaltung für KMUs. Dort ist es bereits möglich die Transaktionen
aus Bankkonten oder PayPal zu importieren. Ein Plugin für Zettle fehlt bislang.
[Hier](https://my.sevdesk.de/swaggerUI/index.html#/) geht es zur Doku.

#### Was ist Zettle?
Zettle ist ein schwedischer Zahlungsdienstleister (wurde von PayPal übernommen) 
welcher u.a. Kassenlösungen für Ladenlokale anbietet.
[Hier](https://github.com/iZettle/api-documentation) geht es zur Doku.

---

### Installation
Über [Composer]() kann das Projekt wie folgt in ein bestehendes Projekt eingebunden werden:
```
composer require zeuch/sevdesk-izettle-integration
```
Alternativ kann man einen Docker Container erstellen. Ein Image liegt im [Docker Hub](https://hub.docker.com/repository/docker/druckdrauf/sevdesk-izettle-integration).
Der daraus resultierende Container führt über einen Cronjob das Snippet [syncPayments](Integration/snippets/syncPayments.php) aus. Damit werden die Verkäufe (Bar/Karte) eines Tages von Zettle in sevDesk importiert.
Dafür sind einige Einstellungen erforderlich. Diese können über Umgebungsvariablen hereingereicht werden.
Alle verfügbaren Einstellungen findet ihr weiter unten unter "Properties".

---

### Beispiele
Unter `/Integration/snippets` findet man ein paar Beispiele wie man die Schnittstelle benutzen kann.

---

### Properties
|Property|Beschreibung|
|----|----|
|MWST|Mehrwertsteuer (z.B. "0.19")|
|LOG_LEVEL|Level des Loggings (Dieses Projekt verwendet Monolog)|
|LOG_PATH|(Optional) Absoluter Pfad für die "Log files". Zusätzlich wird in die Konsole geloggt.|
|SEVDESK_BASEURL|Die URL von der sevDesk API|
|SEVDESK_TOKEN|Der Token (Ist in sevDesk am Benutzer einzusehen)|
|SEVDESK_USERID|Die technische ID des Benutzers, in welchem Kontext die CRUD Aktionen durchgeführt werden sollen. Wird hier genutzt: `\Zeuch\sevDesk\Repo\SevDeskRepo::getDefaultSevUser`|
|SEVDESK_RECHNUNGSNUMMER_REGEX|Optional. Um Rechnungsnummern in Strings zu erkennen, kann man hier einen Regex eintragen. Wird u.a. benötigt, um beim Import eines Verkaufes in das Kassenbuch eine vorhandene Rechnung zu erkennen damit diese dann verbucht werden kann.
|SEVDESK_AUFTRAGSNUMMER_REGEX|Optional. Um Auftragsnummern in Strings zu erkennen, kann man hier einen Regex eintragen. Beim Import eines Barverkaufes in das Kassenbuch wird hiermit die Auftragsnummer erkannt und geschaut, ob es schon eine Rechnung gibt. Wenn ja, wird diese verbucht. Wenn nein, wird eine Rechnung aus dem Auftrag heraus erzeugt und anschließend verbucht.
|IZETTLE_CLIENT_ID|Zettle Client ID|
|IZETTLE_CLIENT_SECRET|Zettle Client Secret|
|IZETTLE_USERNAME|Zettle Benutzername|
|IZETTLE_PASSWORD|Zettle Passwort|
|ZETTLE_KONTO_IN_SEVDESK|Um Kartenzahlungen in sevDesk zu importieren, muss es in sevDesk ein Zahlungskonto vom Typ (online+csv) geben. Die Nummer dieses Zahlungskontos muss dann hier eingetragen werden. Für jeden Verkauf wird ein Eintrag mit dem Betrag des Verkaufes und zusätzlich ein weiterer Eintrag für die Zettle Gebühren erstellt. Der Eintrag für die Gebühren wird direkt mit einem neuen Ausgabe-Beleg verknüpft.|
|ZETTLE_KASSENKONTO_IN_SEVDESK|Um Barzahlungen in sevDesk zu importieren, muss es in sevDesk ein Zahlungskonto vom Typ (online+csv) geben. Barzahlungen werden dann hierhin importiert und zusätzlich wird jeweils eine Umbuchung auf das echte sevDesk Kassenbuch durchgeführt.|
|ZETTLE_LIEFERANT_KUNDENNUMMER|Für die Gebühren von Zettle werden Ausgabe-Belege erzeugt. Dort wird ein Kontakt eingetragen. Sinnvoll ist es hier, in sevDesk einen Kontakt für Zettle vom Typ "Lieferant" anzulegen und die Kundennummer hier einzutragen.|
|SEVDESK_BUCHUNGSKONTO_NUMMER_FUER_ZETTLE_GEBUEHR|Auf dem Ausgabe-Beleg für die Zettle Gebühren muss ein Buchungskonto angegeben werden. Die SKR03/SKR04 Nummer dieses Buchungskontos muss hier angegeben werden. Wir nutzen hierfür ein eigenes Buchungskonto "Nebenkosten des Geldverkehrs" mit der SKR03 Nummer "4970". Die Gebühren werden immer mit 0% MwSt. gebucht. Welches Buchungskonto hier am besten genutzt wird sollte vorher einmal mit dem Steuerberater besprochen werden.|

---
###Funktionsweise
Wie der Import eines Verkaufs und einer Auszahlung von Zettle genau aufgebaut ist und was wann passiert, ist hier einmal in diesem PAP dargestellt.
![Workflow](docs/workflow.png)

---
###Haftungsausschluss
Die Nutzung dieses Projekts geschieht auf eigene Gefahr. Ich übernehme keine Haftung für Schäden o.Ä.