# Stage 1: Anwendung mit Hilfsmitteln zusammenbauen
FROM composer:2.0.8 AS builder
WORKDIR /var/
RUN apk add git
RUN git clone --depth 1 --branch 1.1.7 https://gitlab.com/philippzeuch/sevdesk-izettle-integration.git app
WORKDIR /var/app/
RUN composer install

# Stage 2: Finales Image bauen
FROM php:7.4-cli
LABEL version="1.1.7"
LABEL maintainer="p.zeuch@druck-drauf.de"
WORKDIR /var/www/
COPY --from=builder /var/app .
RUN chown -R www-data:www-data /var/www
RUN apt-get update && apt-get -y install cron

CMD env >> .env_tmp && sed -E 's/(.*=)(.* +.*)/\1"\2"/g' .env_tmp >> .env && rm .env_tmp && echo "${PAYMENT_SYNC_CRON} /usr/local/bin/php /var/www/Integration/snippets/syncPayments.php > /proc/1/fd/1 2>/proc/1/fd/2" | crontab - && cron -f