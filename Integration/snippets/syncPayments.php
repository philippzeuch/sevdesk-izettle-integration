<?php

use Psr\Log\LoggerInterface;
use Zeuch\Integration\Service\TransactionSyncService;

require __DIR__ . '/../standalone.php';

/** @var TransactionSyncService $syncService */
$syncService = $application->getBean(TransactionSyncService::class);
/** @var LoggerInterface $logger */
$logger = $application->getBean(LoggerInterface::class);

// Täglicher Abruf der Verkäufe und Übermittlung nach sevDesk
$now = new DateTime();
$logger->info("syncPayments START " . $now->format("d.M.Y h:s"));

//$syncDate = new DateTime();
//$syncDate->setDate(2021, 05, 26);
//$syncDate->setTime(0, 0);
$syncService->syncForDay(new DateTime("yesterday"));