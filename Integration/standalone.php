<?php
require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Zeuch\Integration\SevDeskZettleIntegration;

// Environment variables
$dotenv = Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();

$application = new SevDeskZettleIntegration($_ENV);