<?php

namespace Zeuch\Integration\Service;

use Psr\Container\ContainerInterface;
use Zeuch\iZettle\Model\iZettleDiscount;
use Zeuch\iZettle\Model\iZettleProdukt;
use Zeuch\iZettle\Model\iZettlePurchase;
use Zeuch\iZettle\Model\ZettlePaymentType;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Repo\ContactRepo;
use Zeuch\sevDesk\Repo\DiscountsRepo;
use Zeuch\sevDesk\Repo\InvoicePosRepo;
use Zeuch\sevDesk\Repo\InvoiceRepo;
use Zeuch\sevDesk\Repo\PaymentMethodRepo;
use Zeuch\sevDesk\Repo\UnityRepo;

class PurchaseService
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Erstellt eine sevDesk Rechnung aus einer Reihe von Zettle-Produkten.<br />
     * Die Rechnung wird ohne Kundenbezug erstellt.
     * @param iZettlePurchase $purchase
     * @param array $produkte
     * @return Invoice
     */
    public function createAndSaveNewInvoiceFromZettlePurchase(iZettlePurchase $purchase, array $produkte) : Invoice
    {
        $invoiceRepo = $this->container->get(InvoiceRepo::class);
        $invoicePosRepo = $this->container->get(InvoicePosRepo::class);
        $unityRepo = $this->container->get(UnityRepo::class);
        $paymentMethodRepo = $this->container->get(PaymentMethodRepo::class);
        $discountRepo = $this->container->get(DiscountsRepo::class);
        $contactRepo = $this->container->get(ContactRepo::class);

        $zahlungsart = $purchase->getPayments()[0]->getType() == ZettlePaymentType::$CASH ? "Barzahlung" : "Kartenzahlung";
        $invoice = $invoiceRepo->createNewInstance();
        $invoice->setHeader("Rechnung Nr. " . $invoice->getInvoiceNumber() . " aus " . $zahlungsart . " (" . $purchase->getPurchaseNumber() . ")");
        $invoice->setFootText("<p>Der Rechnungsbetrag wurde mittels " . $zahlungsart . " vollständig beglichen.</p><p>Mit freundlichen Grüßen<br>[%KONTAKTPERSON%]</p>");
        $invoice->setPaymentMethod($purchase->getPayments()[0]->getType() == ZettlePaymentType::$CASH ?
            $paymentMethodRepo->getBar() : $paymentMethodRepo->getKarte());
        $invoice->setInvoiceDate($purchase->getTimestamp());
        $invoice->setDeliveryDate($purchase->getTimestamp());
        $invoice->setShowNet(false); // Weil wir in der Kasse ja Brutto kassieren und so Rundungsfehler vorbeugen!

        /** @var Invoice $invoice */
        $invoice = $invoiceRepo->saveOrUpdate($invoice);

        $i = 1;
        foreach ($produkte as $produkt) {
            if ($produkt instanceof iZettleProdukt && $produkt->getUnitPrice() > 0) {
                $position = $invoicePosRepo->createNewInstance();
                $position->setQuantity($produkt->getQuantity());
                $position->setName($produkt->getName());
                $position->setText($produkt->getComment());
                $position->setPrice($produkt->getUnitPrice() / 100);
                $position->setTaxRate($produkt->getVatPercentage());
                $position->setUnity($unityRepo->getPieceUnity());
                $position->setPositionNumber($i);
                $position->setInvoice($invoice);

                $invoicePosRepo->saveOrUpdate($position);

                $iZettleDiscount = $produkt->getDiscount();
                if ($iZettleDiscount !== null) {
                    $percentage = $iZettleDiscount->getPercentage();
                    $sevDeskDiscount = $discountRepo->createNewInstance();
                    $sevDeskDiscount->setDiscount(true);
                    $sevDeskDiscount->setObject($invoice);
                    $sevDeskDiscount->setPercentage(false);
                    $sevDeskDiscount->setText("Rabatt " . $iZettleDiscount->getName() . ($percentage !== null ? " (" . $iZettleDiscount->getPercentage() . "%)" : "") . " auf " . $produkt->getName() . " " . $produkt->getComment());

                    // Discounts müssen NETTO hinzugefügt werden
                    if ($percentage !== null) {
                        $brutto = ($produkt->getUnitPrice() * ($percentage / 100)) / 100;
                        $sevDeskDiscount->setValue($brutto / (($produkt->getVatPercentage() / 100) + 1));
                    } else {
                        $sevDeskDiscount->setValue(($iZettleDiscount->getAmount() / 100) / (($produkt->getVatPercentage() / 100) + 1));
                    }
                    $discountRepo->saveOrUpdate($sevDeskDiscount);
                }
            } else if ($produkt instanceof iZettleDiscount) {
                // Separate Discounts müssen NETTO hinzugefügt werden
                for ($j = 0; $j < $produkt->getQuantity(); $j++) {
                    $sevDeskDiscount = $discountRepo->createNewInstance();
                    $sevDeskDiscount->setDiscount(true);
                    $sevDeskDiscount->setObject($invoice);
                    $sevDeskDiscount->setPercentage(false); // TODO hier unterscheiden?? mal schauen...
                    $sevDeskDiscount->setText($produkt->getName());
                    $sevDeskDiscount->setValue($produkt->getAmount() / 100);
                    $discountRepo->saveOrUpdate($sevDeskDiscount);
                }
            }
            $i++;
        }

        return $invoiceRepo->markAsSent($invoice);
    }
}