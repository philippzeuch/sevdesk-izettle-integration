<?php

namespace Zeuch\Integration\Service;

use Psr\Container\ContainerInterface;
use Zeuch\iZettle\Model\iZettlePurchase;

class VerwendungsZweckService
{

    public function __construct(ContainerInterface $c)
    {

    }

    public function generiereVerwendungszweckFuerEinnahme(iZettlePurchase $purchase): string
    {
        $verwendungsZweck = "Verkauf Nr. " . $purchase->getPurchaseNumber() . " | ";
        $produktKommentarString = "";
        foreach ($purchase->getProducts() as $product) {
            $produktKommentarString .= " " . $product->getComment();
        }
        $produktKommentarString = trim($produktKommentarString);

        return $verwendungsZweck . (empty($produktKommentarString) ?: $produktKommentarString . " | ") . $purchase->getPurchaseUUID();
    }

    public function generiereVerwendungszweckFuerGebuehr(iZettlePurchase $purchase): string
    {
        return "Gebühr zu Verkauf Nr. " . $purchase->getPurchaseNumber() . " | " . $purchase->getPurchaseUUID();
    }
}