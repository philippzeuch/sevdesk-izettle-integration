<?php

namespace Zeuch\Integration\Service;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\Integration\Utils\PurchaseImportResult;
use Zeuch\iZettle\Model\iZettlePurchase;
use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Service\CheckAccountTransactionService;
use Zeuch\sevDesk\Service\InvoiceService;
use Zeuch\sevDesk\Utils\CheckAccountImportType;
use Zeuch\sevDesk\Utils\CheckAccountType;

class ZettleKassenKontoService
{
    /** @var ContainerInterface */
    private $container;

    /** @var CheckAccountRepo */
    private $checkAccountRepo;

    private $logger;

    private $transactionRepo;

    private $transactionService;

    private $verwendungsZweckService;

    private $invoiceService;

    public function __construct(ContainerInterface $c)
    {
        $this->checkAccountRepo = $c->get(CheckAccountRepo::class);
        $this->transactionRepo = $c->get(CheckAccountTransactionRepo::class);
        $this->transactionService = $c->get(CheckAccountTransactionService::class);
        $this->verwendungsZweckService = $c->get(VerwendungsZweckService::class);
        $this->invoiceService = $c->get(InvoiceService::class);
        $this->logger = $c->get(LoggerInterface::class);
        $this->container = $c;
    }

    public function importPurchaseFromZettle(iZettlePurchase $purchase): PurchaseImportResult
    {
        $result = new PurchaseImportResult();
        $kontoId = $this->container->get("ZETTLE_KASSENKONTO_IN_SEVDESK");
        $konto = $this->checkAccountRepo->getByBuchungsKontoNummer($kontoId);
        $belegNummer = $purchase->getPurchaseNumber();
        $amount = $purchase->getAmount() / 100;
        $transactionHash = $purchase->getPurchaseUUID();

        if ($konto == null) {
            $result->success = false;
            $msg = "Verkauf Nr. '" . $belegNummer . "' (Barzahlung) kann nicht importiert werden, "
                . "da das Zahlungskonto '" . $kontoId . "' in sevDesk nicht gefunden werden konnte.";
            $result->messages[] = $msg;
            $this->logger->error($msg);
            return $result;
        }

        // Einnahmen-Transaktion erstellen
        $verwendungsZweck = $this->verwendungsZweckService->generiereVerwendungszweckFuerEinnahme($purchase);
        $vorhandeneEinnahmenTransaction = $this->transactionRepo->findByPaymentPurpose($verwendungsZweck);
        if (count($vorhandeneEinnahmenTransaction) < 1) {
            $this->logger->info("Verkauf (Nr. " . $belegNummer . "; " . $amount . " EUR) (Barzahlung) wird importiert.");

            $transaction = $this->transactionService->createNewAndSave($konto, $amount, "", $verwendungsZweck,
                $purchase->getTimestamp(), $transactionHash);

            // Eventuell vorhandene Aufträge (wenn Summen passen) zu Rechnungen machen und direkt verbuchen
            $auftragsNummern = $this->invoiceService->extrahiereAuftragsNummern($verwendungsZweck);
            $this->transactionService->auftraegeVerbuchenAufTransaktion($auftragsNummern, $transaction, $konto);

            // Umbuchung
            $kassenbuch = $this->checkAccountRepo->getKasse();
            $erstellteTransaktionImKassenbuch = null;
            if (!$purchase->getRefund()) {
                // Einnahme
                $transaction = $this->transactionService->createNewAndSave($konto, $amount * -1, "",
                    "Transfer in das Kassenbuch zu Verkauf Nr. " . $belegNummer, $purchase->getTimestamp(), "");

                // Umbuchung vom Kassen(zahlungs-)konto auf das Kassenbuch
                $erstellteTransaktionImKassenbuch = $this->transactionRepo->umbuchen($amount, $purchase->getTimestamp(),
                    $konto, $kassenbuch, $transaction, null);
                $erstellteTransaktionImKassenbuch->setPayeePayerName($konto->getName());
                $erstellteTransaktionImKassenbuch->setPaymtPurpose("Einnahme aus Zettle: Verkauf Nr. " . $belegNummer);
            } else {
                // Ausgabe (Barrückzahlung)
                $transaction = $this->transactionService->createNewAndSave($konto, $amount, "",
                    "Transfer aus dem Kassenbuch zu Verkauf Nr. " . $belegNummer, $purchase->getTimestamp(), "");

                // Umbuchung vom Kassenbuch auf das Kassen(zahlungs-)konto
                $erstellteTransaktionImKassenbuch = $this->transactionRepo->umbuchen($amount, $purchase->getTimestamp(),
                    $kassenbuch, $konto, null, $transaction);
                $erstellteTransaktionImKassenbuch->setPayeePayerName($konto->getName());
                $erstellteTransaktionImKassenbuch->setPaymtPurpose("Bar-Rückzahlung in Zettle: Verkauf Nr. " . $belegNummer);
            }
            $this->transactionRepo->saveOrUpdate($erstellteTransaktionImKassenbuch);

        } else {
            $result->success = false;
            $msg = "Verkauf (Nr. " . $purchase->getPurchaseNumber() . "; " . $amount . " EUR) (Bar) ist bereits importiert worden.";
            $result->messages[] = $msg;
            $this->logger->warning($msg);
        }

        return $result;
    }

    /**
     * Wenn das Zahlungskonto nicht vom Typ "online" ist, dann wird das hier umgestellt.<br />
     * Des Weiteren wird hier der ImportType auf "csv" gestellt.
     */
    public function ensureThatAccountIsTypeOnlineAndCsv()
    {
        $konto = $this->checkAccountRepo->getByBuchungsKontoNummer($this->container->get("ZETTLE_KASSENKONTO_IN_SEVDESK"));
        if ($konto->getType() !== CheckAccountType::$ONLINE) {
            $konto->setType(CheckAccountType::$ONLINE);
            $konto->setImportType(CheckAccountImportType::$CSV);
            $this->checkAccountRepo->saveOrUpdate($konto);
        }
    }
}