<?php

namespace Zeuch\Integration\Service;

use DateTime;
use Psr\Container\ContainerInterface;
use Zeuch\Integration\Utils\AuszahlungenImportResult;
use Zeuch\iZettle\Model\iZettleAccountTransaction;
use Zeuch\iZettle\Model\iZettleAccountTransactionType;
use Zeuch\iZettle\Repo\ZettleTransactionRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Service\CheckAccountTransactionService;

class ZettleAuszahlungImportService
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function auszahlungenImportieren(DateTime $von, DateTime $bis): AuszahlungenImportResult
    {
        $result = new AuszahlungenImportResult();
        $kartenzahlungService = $this->container->get(ZettleKartenzahlungService::class);
        $zettleTransactionRepo = $this->container->get(ZettleTransactionRepo::class);
        $sevDeskTransactionRepo = $this->container->get(CheckAccountTransactionRepo::class);
        $sevDeskTransactionService = $this->container->get(CheckAccountTransactionService::class);
        $kontoId = $this->container->get("ZETTLE_KONTO_IN_SEVDESK");
        $konto = $kartenzahlungService->getKartenzahlungsKonto();

        if (empty($kontoId)) {
            $result->success = false;
            $result->messages[] = "Auszahlungen können nicht importiert werden, "
                . "da kein Zahlungskonto in für den Import in sevDesk spezifiziert ist "
                . "(siehe Property 'ZETTLE_KONTO_IN_SEVDESK').";
            return $result;
        }
        if ($konto == null) {
            $result->success = false;
            $result->messages[] = "Auszahlungen können nicht importiert werden, "
                . "da das Zahlungskonto '" . $kontoId . "' in sevDesk nicht gefunden werden konnte.";
            return $result;
        }

        $auszahlungen = $zettleTransactionRepo->getByZeitspanne($von, $bis, [iZettleAccountTransactionType::$PAYOUT]);
        $result->gesamt = count($auszahlungen);

        foreach ($auszahlungen as $auszahlung) {
            $verwendungsZweck = self::generiereVerwendungszweckFuerAuszahlung($auszahlung);
            $bereitsImportiert = $sevDeskTransactionRepo->findByPaymentPurpose($verwendungsZweck);
            $betrag = $auszahlung->getAmount() / 100;
            if (count($bereitsImportiert) > 0) {
                $result->success = false;
                $result->messages[] = "Auszahlung über " . $betrag . " EUR ist bereits importiert worden.";
                continue;
            }

            $sevDeskTransactionService->createNewAndSave($konto, $betrag,
                "Zettle", $verwendungsZweck, $auszahlung->getTimestamp(), $auszahlung->getOriginatingTransactionUuid());
            $result->gesamtImportiert++;
        }

        return $result;
    }

    public static function generiereVerwendungszweckFuerAuszahlung(iZettleAccountTransaction $auszahlung): string
    {
        // Die UUID packen wir hier mit rein, damit man die Transaktion
        // später in sevDesk eindeutig identifizieren kann (damit wird doppeltes importieren verhindert).

        return "Auszahlung von Zettle | ID: " . $auszahlung->getOriginatingTransactionUuid();
    }
}