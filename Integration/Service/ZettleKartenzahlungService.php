<?php

namespace Zeuch\Integration\Service;

use Psr\Container\ContainerInterface;
use Zeuch\Integration\Utils\PurchaseImportResult;
use Zeuch\iZettle\Model\iZettlePurchase;
use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Service\CheckAccountService;
use Zeuch\sevDesk\Utils\CheckAccountImportType;
use Zeuch\sevDesk\Utils\CheckAccountType;

class ZettleKartenzahlungService extends CheckAccountService
{
    /** @var ZettleGebuehrService */
    private $gebuehrService;

    /** @var ContainerInterface */
    private $container;

    private $verwendungsZweckService;

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
        $this->gebuehrService = $c->get(ZettleGebuehrService::class);
        $this->verwendungsZweckService = $c->get(VerwendungsZweckService::class);
        $this->container = $c;
    }

    public function importPurchaseFromZettle(iZettlePurchase $purchase): PurchaseImportResult
    {
        $result = new PurchaseImportResult();
        $kontoId = $this->container->get("ZETTLE_KONTO_IN_SEVDESK");
        $konto = $this->getKartenzahlungsKonto();
        $belegNummer = $purchase->getPurchaseNumber();
        $amount = $purchase->getAmount() / 100;
        $transactionHash = $purchase->getPurchaseUUID();

        if (empty($kontoId)) {
            $result->success = false;
            $msg = "Verkauf Nr. '" . $belegNummer . "' (Kartenzahlung) kann nicht importiert werden, "
                . "da kein Zahlungskonto in sevDesk für den Import spezifiziert ist (siehe Property 'ZETTLE_KONTO_IN_SEVDESK').";
            $result->messages[] = $msg;
            $this->logger->error($msg);
            return $result;
        }
        if ($konto == null) {
            $result->success = false;
            $msg = "Verkauf Nr. '" . $belegNummer . "' (Kartenzahlung) kann nicht importiert werden, "
                . "da das Zahlungskonto '" . $kontoId . "' in sevDesk nicht gefunden werden konnte.";
            $result->messages[] = $msg;
            $this->logger->error($msg);
            return $result;
        }

        // Einnahmen-Transaktion erstellen
        $verwendungsZweck = $this->verwendungsZweckService->generiereVerwendungszweckFuerEinnahme($purchase);
        $vorhandeneEinnahmenTransaction = $this->transactionRepo->findByPaymentPurpose($purchase->getPurchaseUUID());
        if (count($vorhandeneEinnahmenTransaction) < 1) {
            $this->logger->info("Verkauf (Nr. " . $belegNummer . "; " . $amount . " EUR) (Kartenzahlung) wird importiert.");

            $transaction = $this->transactionService->createNewAndSave($konto, $amount, "",
                $verwendungsZweck, $purchase->getTimestamp(), $transactionHash);

            // Eventuell vorhandene Aufträge (wenn Summen passen) zu Rechnungen machen und direkt verbuchen
            $auftragsNummern = $this->invoiceService->extrahiereAuftragsNummern($verwendungsZweck);
            $this->transactionService->auftraegeVerbuchenAufTransaktion($auftragsNummern, $transaction, $konto);
        } else {
            $result->success = false;
            $msg = "Verkauf (Nr. " . $purchase->getPurchaseNumber() . "; " . $amount . " EUR) (Karte) ist bereits importiert worden.";
            $result->messages[] = $msg;
            $this->logger->warning($msg);
        }

        // Ausgaben-Transaktion + Beleg erstellen (Zettle Gebühr)
        $this->gebuehrService->erstelleGebuehrTransaktionMitBeleg($purchase, $konto, $result);

        return $result;
    }

    /**
     * Wenn das Zahlungskonto nicht vom Typ "online" ist, dann wird das hier umgestellt.<br />
     * Des Weiteren wird hier der ImportType auf "csv" gestellt.
     */
    public function ensureThatAccountIsTypeOnlineAndCsv()
    {
        $konto = $this->getKartenzahlungsKonto();
        if ($konto->getType() !== CheckAccountType::$ONLINE) {
            $konto->setType(CheckAccountType::$ONLINE);
            $konto->setImportType(CheckAccountImportType::$CSV);
            $this->repo->saveOrUpdate($konto);
        }
    }

    public function getKartenzahlungsKonto(): ?CheckAccount
    {
        return $this->repo->getByBuchungsKontoNummer($this->container->get("ZETTLE_KONTO_IN_SEVDESK"));
    }
}