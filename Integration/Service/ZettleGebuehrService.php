<?php

namespace Zeuch\Integration\Service;

use DateTime;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\Integration\Utils\PurchaseImportResult;
use Zeuch\iZettle\Model\iZettleAccountTransaction;
use Zeuch\iZettle\Model\iZettleAccountTransactionType;
use Zeuch\iZettle\Model\iZettlePurchase;
use Zeuch\iZettle\Repo\ZettleTransactionRepo;
use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\Voucher;
use Zeuch\sevDesk\Repo\AccountingTypeRepo;
use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Repo\ContactRepo;
use Zeuch\sevDesk\Repo\VoucherPosRepo;
use Zeuch\sevDesk\Repo\VoucherRepo;
use Zeuch\sevDesk\Service\CheckAccountTransactionService;
use Zeuch\sevDesk\Utils\VoucherStatus;

class ZettleGebuehrService
{
    private $voucherRepo;
    private $logger;
    private $contactRepo;
    private $container;
    private $repo;
    private $accountingTypeRepo;
    private $zettleTransactionRepo;
    private $transactionRepo;
    private $transactionService;
    private $verwendungsZweckService;

    public function __construct(ContainerInterface $c)
    {
        $this->voucherRepo = $c->get(VoucherRepo::class);
        $this->logger = $c->get(LoggerInterface::class);
        $this->contactRepo = $c->get(ContactRepo::class);
        $this->repo = $c->get(CheckAccountRepo::class);
        $this->accountingTypeRepo = $c->get(AccountingTypeRepo::class);
        $this->transactionRepo = $c->get(CheckAccountTransactionRepo::class);
        $this->transactionService = $c->get(CheckAccountTransactionService::class);
        $this->zettleTransactionRepo = $c->get(ZettleTransactionRepo::class);
        $this->verwendungsZweckService = $c->get(VerwendungsZweckService::class);
        $this->container = $c;
    }

    public function erstelleGebuehrTransaktionMitBeleg(iZettlePurchase $purchase, CheckAccount $konto, PurchaseImportResult $result)
    {
        $gebuehrVerwendungsZweck = $this->verwendungsZweckService->generiereVerwendungszweckFuerGebuehr($purchase);
        $vorhandeneGebuehrTransaction = $this->transactionRepo->findByPaymentPurpose($gebuehrVerwendungsZweck);
        if (count($vorhandeneGebuehrTransaction) < 1) {
            $uuid = $purchase->getPayments()[0]->getUuid();
            $zettleGebuehrTransactions = $this->zettleTransactionRepo->getByZeitspanne(
                (clone $purchase->getTimestamp())->modify('-1 day'),
                (clone $purchase->getTimestamp())->modify('+1 day'),
                [iZettleAccountTransactionType::$CARD_PAYMENT_FEE]
            );
            $zettleGebuehrTransactions = array_filter($zettleGebuehrTransactions,
                function (iZettleAccountTransaction $gebuehr) use ($uuid) {
                    return $gebuehr->getOriginatingTransactionUuid() == $uuid && $gebuehr->getAmount() < 0
                        && $gebuehr->getOriginatorTransactionType() == iZettleAccountTransactionType::$CARD_PAYMENT_FEE;
                });
            if (!empty($zettleGebuehrTransactions) && count($zettleGebuehrTransactions) > 0) {
                $zettleGebuehrTransactions = array_values($zettleGebuehrTransactions);
                /** @var iZettleAccountTransaction $iZettleGebuehrTransaction */
                $iZettleGebuehrTransaction = $zettleGebuehrTransactions[0];
                $this->logger->info("Gebühr zu Verkauf Nr. '" . $purchase->getPurchaseNumber() . "' (Kartenzahlung) wird importiert.");

                $transaction = $this->transactionService->createNewAndSave($konto,
                    $iZettleGebuehrTransaction->getAmount() / 100, "Zettle",
                    $gebuehrVerwendungsZweck, $purchase->getTimestamp(), $purchase->getPurchaseUUID());

                $beleg = self::erstelleBelegFuerZettleGebuehr($purchase->getPurchaseNumber(), $purchase->getTimestamp(),
                    ($iZettleGebuehrTransaction->getAmount() / 100) * -1, $result);
                // Beleg Verbuchen
                if ($beleg != null) {
                    $this->voucherRepo->bookAmount($beleg, $iZettleGebuehrTransaction->getAmount() / 100,
                        $purchase->getTimestamp(), $konto, $transaction);
                }
            }
        } else {
            $result->success = false;
            $msg = "Die Gebühr zu Verkauf Nr. '" . $purchase->getPurchaseNumber() . "' ist bereits importiert worden.";
            $result->messages[] = $msg;
            $this->logger->warning($msg);
        }
    }

    public function erstelleBelegFuerZettleGebuehr(string $belegNummer, DateTime $belegDatum, float $summe,
                                                   PurchaseImportResult $result): ?Voucher
    {
        $posRepo = $this->container->get(VoucherPosRepo::class);
        $buchungsKontoNummer = (int)$this->container->get("SEVDESK_BUCHUNGSKONTO_NUMMER_FUER_ZETTLE_GEBUEHR");

        if (empty($buchungsKontoNummer)) {
            $result->success = false;
            $msg = "Um einen Ausgabebeleg für Verkauf Nr. '" . $belegNummer . "' zu erstellen, "
                . "wird die Nummer des Aufwandkontos, "
                . "auf welches der Betrag gebucht werden soll, zwingend benötigt. "
                . "Property: 'SEVDESK_BUCHUNGSKONTO_NUMMER_FUER_ZETTLE_GEBUEHR'.";
            $result->messages[] = $msg;
            $this->logger->error($msg);
            return null;
        }

        $voucher = $this->voucherRepo->createNewInstance();
        $voucher->setDescription("Zettle_" . $belegNummer);
        $voucher->setSupplier($this->contactRepo->getEntities(null, [
            'customerNumber' => $this->container->get("ZETTLE_LIEFERANT_KUNDENNUMMER")
        ])[0]);
        $voucher->setVoucherDate($belegDatum);
        /** @var Voucher $voucher */
        $voucher = $this->repo->saveOrUpdate($voucher);

        $position = $posRepo->createNewInstance();
        $position->setTaxRate(0);
        $position->setComment("Zettle Gebühr");
        $position->setAccountingType($this->accountingTypeRepo->getEigenesBuchungsKontoByNummer($buchungsKontoNummer));
        $position->setVoucher($voucher);
        $position->setIsAsset(0);
        $position->setNet(0);
        $position->setIsGwg(0);
        $position->setSum($summe);
        $posRepo->saveOrUpdate($position);

        // Beleg Status: Entwurf => Offen
        $this->voucherRepo->changeStatus($voucher, VoucherStatus::$DELIVERED);

        return $voucher;
    }
}