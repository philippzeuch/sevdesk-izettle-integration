<?php

namespace Zeuch\Integration\Service;

use DateTime;
use Exception;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\Integration\Utils\PurchaseImportResult;
use Zeuch\iZettle\Model\ZettlePaymentType;
use Zeuch\iZettle\Repo\PurchaseRepo;
use Zeuch\sevDesk\Repo\CheckAccountRepo;

class TransactionSyncService
{
    /** @var PurchaseRepo */
    private $repo;

    /** @var ZettleKassenKontoService */
    private $kassenKontoService;

    /** @var ZettleKartenzahlungService */
    private $kartenzahlungService;

    /** @var LoggerInterface */
    private $logger;

    /** @var CheckAccountRepo */
    private $checkAccountRepo;

    /** @var ZettleAuszahlungImportService */
    private $zettleAuszahlungenImportService;
    /** @var ContainerInterface */
    private $c;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(PurchaseRepo::class);
        $this->kartenzahlungService = $c->get(ZettleKartenzahlungService::class);
        $this->logger = $c->get(LoggerInterface::class);
        $this->checkAccountRepo = $c->get(CheckAccountRepo::class);
        $this->zettleAuszahlungenImportService = $c->get(ZettleAuszahlungImportService::class);
        $this->kassenKontoService = $c->get(ZettleKassenKontoService::class);
        $this->c = $c;
    }

    public function syncForDay(DateTime $day)
    {
        $this->logger->info("Zettle-Schnittstelle: Die Synchronisation wird gestartet.");
        $dayPlusOne = (clone $day)->modify("+1 day");
        $purchases = $this->repo->getByZeitspanne($day, $dayPlusOne);
        $anzahlVerkaeufe = count($purchases);
        $this->logger->info("Es sind " . $anzahlVerkaeufe . " Verkäufe zu importieren!");
        $importierteVerkaeufe = 0;

        // Um Transaktionen auch ohne direkte Verbuchung "offen" zu importieren,
        // sollte das Konto dafür in sevDesk vom Typ "online+csv" sein.
        $this->kartenzahlungService->ensureThatAccountIsTypeOnlineAndCsv();
        if ($this->c->has("ZETTLE_KASSENKONTO_IN_SEVDESK")) {
            $this->kassenKontoService->ensureThatAccountIsTypeOnlineAndCsv();
        }

        $importResults = array();
        foreach ($purchases as $purchase) {
            $payment = $purchase->getPayments()[0];
            $purchaseNumber = $purchase->getPurchaseNumber();
            if ($payment !== null) {
                $result = new PurchaseImportResult();
                try {
                    switch ($payment->getType()) {
                        case ZettlePaymentType::$CARD:
                            // Zettle Kartenzahlungskonto in sevDesk
                            $result = $this->kartenzahlungService->importPurchaseFromZettle($purchase);
                            break;
                        case ZettlePaymentType::$CASH:
                            if ($this->c->has("ZETTLE_KASSENKONTO_IN_SEVDESK") && !empty($this->c->get("ZETTLE_KASSENKONTO_IN_SEVDESK"))) {
                                // Alternatives "Kassenkonto" in sevDesk
                                $result = $this->kassenKontoService->importPurchaseFromZettle($purchase);
                            } else {
                                // Kassenbuch in sevDesk
                                $this->logger->warning("Der Bar-Verkauf Nr. {vnr} kann nicht importiert werden, 
                                    da die Property '{p}' ist nicht angegeben ist.", [
                                        'vnr' => $purchaseNumber, '
                                        p'    => 'ZETTLE_KASSENKONTO_IN_SEVDESK',
                                ]);
                            }
                            break;
                        case ZettlePaymentType::$GIFTCARD:
                        case ZettlePaymentType::$PAYPAL:
                            // TODO muss noch implementiert werden...
                            break;
                    }
                    if ($result->success) {
                        $this->logger->info("Verkauf Nr. " . $purchaseNumber . " wurde erfolgreich importiert.");
                        $importierteVerkaeufe++;
                    }
                } catch (Exception $e) {
                    $this->logger->error("Ein Zettle-Datensatz wurde nicht korrekt importiert: " . $e->getMessage());
                    $result->success = false;
                    $result->messages[] = "Verkauf Nr. " . $purchaseNumber . " => " . $e->getMessage();
                }
                $importResults[] = $result;
            }
        }

        // Transaktionen automatisch zuordnen lassen
        $karzenzahlungsKonto = $this->kartenzahlungService->getKartenzahlungsKonto();
        if ($karzenzahlungsKonto !== null) {
            $this->checkAccountRepo->transaktionenAutomatischZuordnen($karzenzahlungsKonto);
        }
        if ($this->c->has("ZETTLE_KASSENKONTO_IN_SEVDESK") && !empty($this->c->get("ZETTLE_KASSENKONTO_IN_SEVDESK"))) {
            $konto = $this->checkAccountRepo->getByBuchungsKontoNummer($this->c->get("ZETTLE_KASSENKONTO_IN_SEVDESK"));
            if ($konto !== null) {
                $this->checkAccountRepo->transaktionenAutomatischZuordnen($konto);
            }
        }

        // Auszahlungen von Zettle als Abgang auf dem Zettle-Konto in sevDesk erstellen
        $auszahlungenImportResult = $this->zettleAuszahlungenImportService->auszahlungenImportieren($day, $dayPlusOne);

        $this->logger->info("Zettle Schnittstelle: Synchronisation beendet. "
            . "Es wurden " . $importierteVerkaeufe . " von " . $anzahlVerkaeufe . " Verkäufe importiert. "
            . "Es wurden " . $auszahlungenImportResult->gesamtImportiert . " von " . $auszahlungenImportResult->gesamt
            . " Auszahlungen importiert.");

        // TODO Mail verschicken!
    }
}