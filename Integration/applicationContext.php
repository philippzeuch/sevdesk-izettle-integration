<?php

use DI\Definition\Helper\FactoryDefinitionHelper;
use Psr\Container\ContainerInterface;
use Zeuch\Integration\Service\PurchaseService;
use Zeuch\Integration\Service\TransactionSyncService;
use Zeuch\Integration\Service\VerwendungsZweckService;
use Zeuch\Integration\Service\ZettleAuszahlungImportService;
use Zeuch\Integration\Service\ZettleGebuehrService;
use Zeuch\Integration\Service\ZettleKartenzahlungService;
use Zeuch\Integration\Service\ZettleKassenKontoService;
use Zeuch\iZettle\iZettleConnection;
use Zeuch\iZettle\Repo\ZettleTransactionRepo;
use Zeuch\iZettle\Repo\PurchaseRepo;
use Zeuch\iZettle\Service\iZettleDiscountService;
use Zeuch\iZettle\Service\iZettleProduktService;
use Zeuch\iZettle\Service\ZettlePurchaseService;

return [
    // Services
    iZettleDiscountService::class => DI\create(iZettleDiscountService::class),
    iZettleProduktService::class => DI\create(iZettleProduktService::class),
    ZettlePurchaseService::class => DI\create(ZettlePurchaseService::class)->constructor(DI\get(ContainerInterface::class)),
    PurchaseService::class => DI\create(PurchaseService::class)->constructor(DI\get(ContainerInterface::class)),
    TransactionSyncService::class => DI\create(TransactionSyncService::class)->constructor(DI\get(ContainerInterface::class)),
    ZettleKartenzahlungService::class => DI\create(ZettleKartenzahlungService::class)->constructor(DI\get(ContainerInterface::class)),
    ZettleGebuehrService::class => DI\create(ZettleGebuehrService::class)->constructor(DI\get(ContainerInterface::class)),
    ZettleKassenKontoService::class => DI\create(ZettleKassenKontoService::class)->constructor(DI\get(ContainerInterface::class)),
    ZettleAuszahlungImportService::class => DI\create(ZettleAuszahlungImportService::class)->constructor(DI\get(ContainerInterface::class)),
    VerwendungsZweckService::class => DI\create(VerwendungsZweckService::class)->constructor(DI\get(ContainerInterface::class)),
    iZettleConnection::class => new FactoryDefinitionHelper(function (ContainerInterface $c) {
        return new iZettleConnection($c->get('IZETTLE_CLIENT_ID'), $c->get('IZETTLE_CLIENT_SECRET'),
            $c->get('IZETTLE_USERNAME'), $c->get('IZETTLE_PASSWORD'));
    }),

    // Repos
    ZettleTransactionRepo::class => DI\create(ZettleTransactionRepo::class)->constructor(DI\get(ContainerInterface::class)),
    PurchaseRepo::class => DI\create(PurchaseRepo::class)->constructor(DI\get(ContainerInterface::class))
];
