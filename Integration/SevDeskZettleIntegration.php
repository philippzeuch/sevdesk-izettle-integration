<?php

namespace Zeuch\Integration;

use Zeuch\sevDesk\DefaultSevDeskApplicationContext;
use Psr\Container\ContainerInterface;
use DI\ContainerBuilder;
use Psr\Log\LoggerInterface;
use Zeuch\iZettle\iZettleConnection;

class SevDeskZettleIntegration
{
    /** @var array */
    private $config;

    /** @var ContainerInterface */
    private $container;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->setUpDependencyInjection();

        $this->initConnectionToZettle();

        // Für das Logging nehmen wir den Logger, welcher aus dem sevdesk-sdk kommt, da dieser ausreicht.
        $logger = $this->container->get(LoggerInterface::class);
        $logger->info("sevdesk-zettle-integration initialized!");
    }

    private function setUpDependencyInjection()
    {
        // Container config
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(false);
        $containerBuilder->useAnnotations(false);
        $containerBuilder->addDefinitions(__DIR__ . "/applicationContext.php");
        $containerBuilder->addDefinitions(DefaultSevDeskApplicationContext::get());
        $containerBuilder->addDefinitions($this->config);
        $this->container = $containerBuilder->build();
    }

    public function getBean(string $beanClassName)
    {
        return $this->container->get($beanClassName);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    private function initConnectionToZettle()
    {
        $this->container->get(iZettleConnection::class)->connect();
    }
}