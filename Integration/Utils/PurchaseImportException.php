<?php

namespace Zeuch\Integration\Utils;

use Exception;

/**
 * Wird geworfen, sobald beim importieren in sevDesk irgendwelche fachlichen Fehler auftreten
 *
 * @package Zeuch\Integration\Utils
 */
class PurchaseImportException extends Exception
{
    /** @var PurchaseImportResult */
    private $result;

    public function __construct(PurchaseImportResult $result)
    {
        parent::__construct(implode("\n", $result->messages));
        $this->result = $result;
    }

    public function getResult(): PurchaseImportResult
    {
        return $this->result;
    }
}