<?php

namespace Zeuch\Integration\Utils;

class AuszahlungenImportResult
{
    /** @var int  */
    public $gesamt = 0;

    /** @var int  */
    public $gesamtImportiert = 0;

    /** @var bool  */
    public $success = true;

    /** @var string[] */
    public $messages = array();
}