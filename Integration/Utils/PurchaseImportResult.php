<?php

namespace Zeuch\Integration\Utils;

class PurchaseImportResult
{
    /** @var bool */
    public $success = true;

    /** @var string[] */
    public $messages = array();
}